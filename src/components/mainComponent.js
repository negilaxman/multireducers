import React, { Component } from "react";
import Menu from "../components/MenuComponent";
//import { DISHES } from "../shared/dishes";
//import { COMMENTS } from "../shared/comments";
//import { PROMOTIONS } from "../shared/promotions";
//import { LEADERS } from "../shared/leaders";
import Header from "../components/HeaderComponent";
import Footer from "../components/FooterComponent";
import { Route, Switch, Redirect, withRouter } from "react-router-dom";
import Home from "../components/HomeComponent";
import ContactComponent from "../components/ContactComponent";
import DishDetail from "./DishdetailComponent";
import About from "../components/AboutComponent";
import { connect } from "react-redux";
import Enquiry from "../components/Enquiry";

const mapStateToProps = state => {
    return {
        dishes: state.dishes,
        comments: state.comments,
        promotions: state.promotions,
        leaders: state.leaders,
    };
};

class Main extends Component {
    constructor(props) {
        super(props);
        /* this.state = {
            dishes: DISHES,
            comments: COMMENTS,
            promotions: PROMOTIONS,
            leaders: LEADERS,
        };*/
    }

    render() {
        const DishWithId = ({ match }) => {
            return (
                <DishDetail
                    dish={
                        this.props.dishes.filter(
                            dish =>
                                dish.id === parseInt(match.params.dishId, 10)
                        )[0]
                    }
                    comments={this.state.comments.filter(
                        comment =>
                            comment.dishId === parseInt(match.params.dishId, 10)
                    )}
                />
            );
        };

        const HomePage = () => {
            return (
                <Home
                    dish={this.props.dishes.filter(dish => dish.featured)[0]}
                    promotion={
                        this.props.promotions.filter(promo => promo.featured)[0]
                    }
                    leader={
                        this.props.leaders.filter(leader => leader.featured)[0]
                    }
                />
            );
        };
        return (
            <div className="App">
                <Header />
                <Switch>
                    <Route path="/home" component={HomePage} />
                    <Route
                        exact
                        path="/menu"
                        component={() => <Menu dishes={this.props.dishes} />}
                    />
                    <Route exact path="/menu/:dishId" component={DishWithId} />
                    <Route path="/contact" component={ContactComponent} />
                    <Route
                        path="/About"
                        component={() => <About leaders={this.props.leaders} />}
                    />
                    <Route path="/enquiry" component={Enquiry} />
                    <Redirect to="/home" />
                </Switch>
                <Footer />
            </div>
        );
    }
}

//export default Main;
export default withRouter(connect(mapStateToProps)(Main));
