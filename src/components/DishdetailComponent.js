import React from "react";
import { Menu } from "./MenuComponent";
import { debug } from "util";
//import Menu from "./MenuComponent";
import {
    Media,
    Card,
    CardImg,
    CardTitle,
    CardBody,
    CardImgOverlay,
    CardText,
} from "reactstrap";
import { Link } from "react-router-dom";

class DishDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            item: null,
        };
    }
    renderDish(dish) {
        if (dish != null) {
            return (
                <Card>
                    <Link to={`/menu/${dish.id}`}>
                        <CardImg
                            width="100%"
                            src={dish.image}
                            alt={dish.name}
                        />
                        <CardBody>
                            <CardTitle>{dish.name}</CardTitle>
                            <CardText>{dish.description}</CardText>
                        </CardBody>
                    </Link>
                </Card>
            );
        } else {
            return <div>Dish not found</div>;
        }
    }
    render() {
        return (
            <div>
                <div>{this.renderDish(this.props.data)}</div>
            </div>
        );
    }
}
export default DishDetail;
