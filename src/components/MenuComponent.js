import React, { Component } from "react";
import DishDetail from "./DishdetailComponent";
import {
    Media,
    Card,
    CardImg,
    CardTitle,
    CardBody,
    CardImgOverlay,
    CardText,
    Breadcrumb,
    BreadcrumbItem,
} from "reactstrap";
import { Link } from "react-router-dom";

class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedDish: null,
        };
    }

    onSelectDish(dish) {
        //console.log(dish);
        this.setState({
            selectedDish: dish,
        });
    }

    render() {
        const menu = this.props.dishes.map(dish => {
            return (
                <div key={dish.id} className="col-12 col-md-3 m-1">
                    <Card
                        onClick={() => {
                            this.onSelectDish(dish);
                        }}
                    >
                        <CardImg
                            width="100%"
                            src={dish.image}
                            alt={dish.name}
                        />
                        <CardImgOverlay>
                            <CardTitle heading>{dish.name}</CardTitle>
                        </CardImgOverlay>
                    </Card>
                </div>
            );
        });
        return (
            <div className="container">
                <div className="row">{menu}</div>
                <div className="row">
                    {/* <div className="col-12 col-md-5 m-1">
                        {this.renderDish(this.state.selectedDish)}
                    </div> */}
                    <div className="col-12 col-md-5 m-1">
                        <DishDetail data={this.state.selectedDish} />
                    </div>
                </div>
            </div>
        );
    }
}

export default Menu;
