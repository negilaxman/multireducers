import { createStore, combineReducers } from "redux";
import { Dishes } from "./Dishes";
import { Comments } from "./Comments";
import { Promotions } from "./Promotions";
import { Leaders } from "./Leaders";

//import { Reducer, initialState } from "./reducer";

//export const configureStore = () => {
//  const store = createStore(Reducer, initialState);
//return store;
//};

export const configureStore = () => {
    const store = createStore(
        combineReducers({
            dishes: Dishes,
            comments: Comments,
            promotions: Promotions,
            leaders: Leaders,
        })
    );
    return store;
};
